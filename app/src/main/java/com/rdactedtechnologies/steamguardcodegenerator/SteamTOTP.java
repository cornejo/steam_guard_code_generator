package com.rdactedtechnologies.steamguardcodegenerator;

import android.support.annotation.NonNull;

import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

class SteamTOTP
{
    private final static String STEAM_CHARS[] = { "2", "3", "4", "5", "6", "7", "8", "9", "B", "C", "D", "F", "G",
                                                  "H", "J", "K", "M", "N", "P", "Q", "R", "T", "V", "W", "X", "Y"};

    private final static int LONG_BYTES_SIZE = 8;
    private final static int INT_BYTES_SIZE = 4;

    @NonNull
    private static byte[] longToBytes(long l)
    {
        // TODO endian??
        return ByteBuffer.allocate(LONG_BYTES_SIZE).putLong(l).array();
    }

    private static int bytesToInt(byte[] b)
    {
        // TODO endian??
        return ByteBuffer.wrap(b).getInt();
    }

    private static byte[] hmacSha1(byte[] value, byte[] key)
            throws NoSuchAlgorithmException,
            InvalidKeyException
    {
        String type = "HmacSHA1";
        SecretKeySpec secret = new SecretKeySpec(key, type);
        Mac mac = Mac.getInstance(type);
        mac.init(secret);
        return mac.doFinal(value);
    }

    /*
    private static void printHexBytes(byte[] b)
    {
        for(int i = 0; i < b.length; i++)
        {
            System.out.print("0x" + Integer.toHexString(b[i] & 0xff) + " ");
        }
        System.out.println();
    }
    */

    static String generateSteamCode(long intervalCount, byte[] secret)
    {
        byte[] intervalCountBytes = longToBytes(intervalCount);

        /*
        System.out.print("value: ");
        printHexBytes(intervalCountBytes);
        */

        try
        {
            byte[] digest = hmacSha1(intervalCountBytes, secret);

            /*
            System.out.println("digest: ");
            printHexBytes(hmacResult);
            */

            int start = digest[19] & 0x0f;

            // System.out.println("start: " + start);

            byte[] bytes = new byte[INT_BYTES_SIZE];
            System.arraycopy(digest, start, bytes, 0, INT_BYTES_SIZE);

            bytes[0] = (byte)(bytes[0] & 0x7f);

            /*
            System.out.print("bytes: ");
            printHexBytes(bytes);
            */

            int fullcode = bytesToInt(bytes);

            //System.out.println("fullcode: " + Long.toHexString(fullcode));

            StringBuilder codeBuffer = new StringBuilder();
            for(int i = 0; i < 5; i++)
            {
                int offset = fullcode % STEAM_CHARS.length;
                //System.out.println(offset);
                codeBuffer.append(STEAM_CHARS[offset]);
                fullcode /= STEAM_CHARS.length;
            }

            return codeBuffer.toString();
        } catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        } catch (InvalidKeyException e)
        {
            e.printStackTrace();
        }

        return "Code generation failed";
    }
}
