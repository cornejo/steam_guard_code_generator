package com.rdactedtechnologies.steamguardcodegenerator;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;

import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;

public class MainActivity extends AppCompatActivity
{
    private final String APP_PREFERENCE_CONTAINER = "Steam_Accounts";
    private static final String ACCOUNTS_PREF_KEY = "Accounts";
    private final int STEAM_SECRET_KEY_LENGTH = 32;
    private final int INTERVAL_PERIOD_SECS = 30;
    private final int REFRESH_INTERVAL = 1000;
    private Runnable updateRunner = null;
    private Set<String> accounts = null;
    private final Handler updateHandler = new Handler();
    private final Hashtable<String, SteamCodeView> accountViewList = new Hashtable<>();

    private long getIntervalCount()
    {
        // Returns number of 30-second intervals elapsed since epoch
        return (System.currentTimeMillis() / 1000) / INTERVAL_PERIOD_SECS;
    }

    private int getSecondsTillChange()
    {
        // Returns number of seconds until the next change
        return INTERVAL_PERIOD_SECS - (int) (System.currentTimeMillis() / 1000) % INTERVAL_PERIOD_SECS;
    }

    private void presentAccount(String account, String accountName, String code, int secondsRemaining)
    {
        final TableLayout table = this.findViewById(R.id.AccountTable);

        SteamCodeView steamCodeView = accountViewList.get(account);
        if(steamCodeView == null)
        {
            // It doesn't exist, create it
            steamCodeView = new SteamCodeView(this, accountName);
            table.addView(steamCodeView);

            accountViewList.put(account, steamCodeView);

            steamCodeView.setLongClickable(true);
            steamCodeView.setOnLongClickListener(new View.OnLongClickListener()
            {
                @Override
                public boolean onLongClick(View v)
                {
                    final View finalTextView = v;
                    AlertDialog.Builder builder;
                    builder = new AlertDialog.Builder(v.getContext());
                    builder.setTitle("Delete entry")
                            .setMessage("Are you sure you want to delete this entry?")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // continue with delete
                                    // Clone the keys so we can remove while iterating
                                    Set<String> keys = new HashSet<>(accountViewList.keySet());
                                    for(String key: keys)
                                    {
                                        if( finalTextView == accountViewList.get(key))
                                        {
                                            accountViewList.remove(key);
                                            accounts.remove(key);
                                            table.removeView(finalTextView);
                                        }
                                    }

                                    saveAccountData();
                                }
                            })
                            .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // do nothing
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                    return true;
                }
            });
        }

        steamCodeView.setCode(code, secondsRemaining);
    }

    private void updateAccount(String account, long intervalCount, int secondsRemaining)
    {
        String code;

        String accountName = account.substring(0, account.length() - (STEAM_SECRET_KEY_LENGTH + 1));
        String accountSecret = account.substring(account.length() - STEAM_SECRET_KEY_LENGTH);

        if (account.charAt(account.length() - (STEAM_SECRET_KEY_LENGTH + 1)) == ':')
        {
            try
            {
                byte[] secret = Base32String.decode(accountSecret);

                // The authentication code
                code = SteamTOTP.generateSteamCode(intervalCount, secret);

                // System.out.println(accountName + " + " + accountSecret + " -> " + code);
            } catch (Base32String.DecodingException e)
            {
                code = "Base32 exception: " + e;
            }
        }
        else
        {
            code = "Bad account details";
        }

        presentAccount(account, accountName, code, secondsRemaining);
    }

    private void addAccount(String accountName, String accountSecret)
    {
        accounts.add(accountName + ":" + accountSecret);

        // We will automatically create the visual elements the next time the tokens are updated
        updateTokens();

        saveAccountData();
    }

    private boolean areFieldsValid(String accountName, String accountSecret)
    {
        if (accountName == null || accountSecret == null)
        {
            return false;
        }

        // Must have a non-zero account name, and a secret of appropriate length
        if (accountName.length() == 0 || accountSecret.length() != STEAM_SECRET_KEY_LENGTH)
        {
            return false;
        }

        try
        {
            // Ensure the secret does not have any invalid characters
            Base32String.decode(accountSecret);
        } catch (Base32String.DecodingException e)
        {
            return false;
        }

        return true;
    }

    public void onAddButtonClick(View v)
    {
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(v.getContext());

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        @SuppressLint("InflateParams") final View dialogView = inflater.inflate(R.layout.account_registration, null);

        // Get the text fields
        final EditText accountName = dialogView.findViewById(R.id.accountName);
        final EditText accountSecret = dialogView.findViewById(R.id.accountSecret);

        // Build the dialog asking for account information
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setView(dialogView)
                // Add action buttons
                .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int id)
                    {
                        // Validate the data - if valid, add it to the account list
                        String accountNameText = accountName.getText().toString();
                        String accountSecretText = accountSecret.getText().toString();

                        addAccount(accountNameText, accountSecretText);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener()
                {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        dialog.cancel();
                    }
                });

        // Create and show the dialog
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        // Disable the OK button
        final Button positiveButton = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
        positiveButton.setEnabled(false);

        // Add a watcher to enable the OK button whenever the information seems sane
        TextWatcher watcher = new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after)
            {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (areFieldsValid(accountName.getText().toString(), accountSecret.getText().toString()))
                {
                    positiveButton.setEnabled(true);
                }
                else
                {
                    positiveButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s)
            {
                // Nothing to do
            }
        };

        accountName.addTextChangedListener(watcher);
        accountSecret.addTextChangedListener(watcher);
    }

    private void updateTokens()
    {
        // Number of 30-second intervals since epoch
        long intervalCount = getIntervalCount();
        // Number of seconds till the next change
        int secondsRemaining = getSecondsTillChange();

        // System.out.println(intervalCount);

        for (String account : accounts)
        {
            updateAccount(account, intervalCount, secondsRemaining);
        }
    }

    private void saveAccountData()
    {
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(APP_PREFERENCE_CONTAINER, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.putStringSet(ACCOUNTS_PREF_KEY, accounts);
        editor.apply();
    }

    private void loadAccountData()
    {
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(APP_PREFERENCE_CONTAINER, MODE_PRIVATE);
        accounts = sharedPref.getStringSet(ACCOUNTS_PREF_KEY, new HashSet<String>());
    }

    @Override
    public void onResume()
    {
        super.onResume();

        updateRunner = new Runnable()
        {
            @Override
            public void run()
            {
                updateTokens();

                // Check to see if we should continue to update the tokens
                if(this == updateRunner)
                {
                    updateHandler.postDelayed(this, REFRESH_INTERVAL);
                }
            }
        };

        updateHandler.postDelayed(updateRunner, REFRESH_INTERVAL);
    }

    @Override
    public void onPause()
    {
        super.onPause();

        // This will stop the updater the next time it runs
        updateRunner = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Move the button to the foreground, otherwise it is not clickable
        ImageButton addButton = findViewById(R.id.addButton);
        addButton.bringToFront();
        addButton.invalidate();

        loadAccountData();

        updateTokens();
    }
}
