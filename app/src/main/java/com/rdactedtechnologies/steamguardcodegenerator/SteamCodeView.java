package com.rdactedtechnologies.steamguardcodegenerator;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.AppCompatTextView;

public class SteamCodeView extends AppCompatTextView
{
    private String accountName;

    public SteamCodeView(Context context)
    {
        super(context);

        init("UNSPECIFIED");
    }

    public SteamCodeView(Context context, String accountName)
    {
        super(context);

        init(accountName);
    }

    private void init(String accountName)
    {
        this.accountName = accountName;
        this.setTextSize(40);
    }

    public void setCode(String code, int secondsRemaining)
    {
        Resources res = getResources();
        String text = res.getString(R.string.code_presentation, code, accountName);
        this.setText(text);
    }
}
